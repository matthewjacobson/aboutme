# About Me

- Made with [Hugo](https://gohugo.io)
- Used the [Uilite](https://themes.gohugo.io/hugo-uilite/) theme as a base, modified slightly
- Used most the `.gitlab-ci.yml` from the GitLab [example](https://gitlab.com/pages/hugo/blob/master/.gitlab-ci.yml)

### To Run Local:
- Install Hugo
```
brew install hugo
```

- Clone repo, then:
```
hugo server
```

- Using a PC or need more info, Hugo site [here](https://gohugo.io/getting-started/quick-start/)

### Logo credit:
* Logo for repo is from https://fontawesome.com
* No changes were made to the logo
* Link to the license: https://fontawesome.com/license
